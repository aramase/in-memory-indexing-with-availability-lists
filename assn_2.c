/*four fields: SID | last | first | major

SID is unique for every member - Primary key
Size of record mentioned as int before record 

availability list order and name of student file
1. first fit - same order saved
2. best fit - ascending order of hole size
3. worst fit - descending order of hole size

*/



#include <stdio.h>

char *buf; /* Buffer to hold student record */ 

FILE *fp; /* Input/output stream */ 
long rec_off; /* Record offset */ 
int rec_siz; /* Record size, in characters */ 

/* If student.db doesn't exist, create it, otherwise read * its first record */ 

if ( ( fp = fopen( "student.db", "r+b" ) ) == NULL ) 
	{ 
		fp = fopen( "student.db", "w+b" ); 
	} else { 
		rec_off = 0; 
		fseek( fp, rec_off, SEEK_SET ); 
		fread( &rec_siz, sizeof( int ), 1, fp ); 
		buf = malloc( rec_siz + 1 ); 
		fread( buf, 1, rec_siz, fp ); 
		buf[ rec_siz ] = '\0'; 
	}
}

//to maintain key index

#include <stdio.h> 

typedef struct{ 
	int key; /* Record's key */ 
	long off; /* Record's offset in file */ 
} index_S; 

FILE *out; /* Output file stream */ 
index_S prim[ 50 ]; /* Primary key index */ 
out = fopen( "index.bin", "wb" ); 
fwrite( prim, sizeof( index_S ), 50, out ); 
fclose( out );